from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import ShortUrl
from django.urls import NoReverseMatch

PORT = 5555


def whole_url(url):
    if not(url.startswith('http://') or url.startswith('https://')):
        url = "https://" + url
    return url


def search_url(url):
    try:
        url = ShortUrl.objects.get(url=url)
        return True
    except ShortUrl.DoesNotExist:
        return False


def shorten_url(url, content):
    if not search_url(url):
        shortened = "http://localhost:" + str(PORT) + "/" + content
        while ShortUrl.objects.filter(url=shortened).exists():
            if content.isdigit():
                content = str(int(content) + 1)
            shortened = "http://localhost:" + str(PORT) + "/" + content
        return shortened


def index(request):
    if request.method == "GET":
        url_list = ShortUrl.objects.all()
        print(url_list)
        return render(request, "index.html", {'url_list': url_list})

    elif request.method == "POST":
        url = request.POST.get('url')
        content = request.POST.get('shortened')
        if not content:
            content = str(ShortUrl.objects.count() + 1)
        shortened = shorten_url(url, content)

        if shortened:
            ShortUrl.objects.create(url=url, shortened=shortened, content=content)
        print(url)
        print(shortened)
        print(content)
    url_list = ShortUrl.objects.all()
    return render(request, 'index.html', {'url_list': url_list})


def redirect_url(request, content):
    try:
        print("MI CONTENT ES" + content)
        url = ShortUrl.objects.get(content=content)
        print("MI URL ES " + url.url)
        return redirect(url.url)
    except ShortUrl.DoesNotExist:
        return redirect('index')

